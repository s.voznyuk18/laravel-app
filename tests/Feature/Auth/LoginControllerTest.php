<?php

namespace Tests\Feature\Auth;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\Feature\Traits\SetUpTrait;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use SetUpTrait;

    public function test_it_logs_in_user_with_valid_credentials()
    {
        $user = User::factory()->create([
            'email' => 'test@test.com',
            'password' => Hash::make('test1234')
        ]);

        $response = $this->post(route('login'), [
            'email' => 'test@test.com',
            'password' => 'test1234'
        ]);
        $response->assertRedirect('/home');

        $this->assertAuthenticatedAs($user);
    }

    public function test_it_does_not_log_in_user_with_invalid_password()
    {
        User::factory()->create([
            'email' => 'test@test.com',
            'password' => Hash::make('test1234')
        ]);

        $this->post(route('login'), [
            'email' => 'test@test.com',
            'password' => 'wrongpass'
        ]);

        $this->assertGuest();
    }

    public function test_it_does_not_log_in_user_with_invalid_email()
    {
        User::factory()->create([
            'email' => 'test@test.com',
            'password' => Hash::make('test1234')
        ]);

        $this->post(route('login'), [
            'email' => 'wrong@test.com',
            'password' => 'test1234'
        ]);

        $this->assertGuest();
    }

    public function test_admin_can_access_admin_panel()
    {
        $response = $this->actingAs($this->user())->get(route('admin.dashboard'));
        $response->assertSuccessful();
        $response->assertViewIs('admin.dashboard');
    }

    public function test_moderator_can_access_admin_panel()
    {
        $response = $this->actingAs($this->user(Role::MODERATOR))->get(route('admin.dashboard'));
        $response->assertSuccessful();
        $response->assertViewIs('admin.dashboard');
    }

    public function test_customer_can_not_access_admin_panel()
    {
        $response = $this->actingAs($this->user(Role::CUSTOMER))->get(route('admin.dashboard'));
        $response->assertForbidden();
    }
}
