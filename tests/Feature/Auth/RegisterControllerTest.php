<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\Traits\SetUpTrait;
use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    use SetUpTrait;

    public function test_it_creates_user_with_valid_data()
    {
        $data = User::factory()->makeOne()->toArray();
        $data['password'] = 'password123';
        $data['password_confirmation'] = 'password123';

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);

        $response = $this->post(route('register'), $data);
        $response->assertStatus(302);
        $response->assertRedirect('/');

        $this->assertDatabaseHas(User::class, ['email' => $data['email']]);
    }

    public function test_does_not_create_user_with_invalid_name()
    {
        $data = User::factory()->makeOne()->toArray();
        $data['name'] = '';
        $data['password'] = 'password123';
        $data['password_confirmation'] = 'password123';

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);

        $response = $this->post(route('register'), $data);
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['name']);

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);
    }

    public function test_does_not_create_user_with_invalid_lastname()
    {
        $data = User::factory()->makeOne()->toArray();
        $data['lastName'] = '';
        $data['password'] = 'password123';
        $data['password_confirmation'] = 'password123';

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);

        $response = $this->post(route('register'), $data);
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['lastName']);

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);
    }

    public function test_does_not_create_user_with_invalid_email()
    {
        $data = User::factory()->makeOne()->toArray();
        $data['email'] = 'invalidEmail';
        $data['password'] = 'password123';
        $data['password_confirmation'] = 'password123';

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);

        $response = $this->post(route('register'), $data);
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['email']);

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);
    }

    public function test_does_not_create_user_with_unconfirmed_password()
    {
        $data = User::factory()->makeOne()->toArray();
        $data['password'] = 'password123';
        $data['password_confirmation'] = 'passwor';

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);
        $this->assertNotEquals($data['password'], $data['password_confirmation']);

        $response = $this->post(route('register'), $data);
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['password']);

        $this->assertDatabaseMissing(User::class, ['email' => $data['email']]);
    }
}
