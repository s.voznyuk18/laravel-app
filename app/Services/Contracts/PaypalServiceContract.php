<?php

declare(strict_types=1);

namespace App\Services\Contracts;

use App\Enums\TransactionStatus;
use App\Http\Requests\CreateOrderRequest;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Http\JsonResponse;

interface PaypalServiceContract
{
    public function create(Cart $cart): string|null;
    public function capture(string $vendorOrderId): TransactionStatus;
}
