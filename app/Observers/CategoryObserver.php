<?php

namespace App\Observers;

use App\Models\Category;

class CategoryObserver
{
    public function deleted(Category $category): void
    {
        if ($category->child()->exists()) {
            $category->child()->update(['parent_id' => null]);
        }
    }
}
