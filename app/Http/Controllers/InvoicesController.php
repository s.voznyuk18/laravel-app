<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Services\Contracts\InvoiceServiceContract;
use Illuminate\Http\Request;

class InvoicesController extends Controller
{
    public function __invoke(Order $order, InvoiceServiceContract $invoiceService)
    {
        return $invoiceService->generate($order)->stream();
    }
}
