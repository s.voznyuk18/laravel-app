<?php

namespace App\Listeners;

use Gloudemans\Shoppingcart\Facades\Cart;

class SaveCartOnLogOut
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        if (Cart::instance('cart')->count() > 0) {
            Cart::instance('cart')->store($event->user->id . '_cart');
        }
    }
}
