<?php

declare(strict_types=1);


namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface ImagesRepositoryContract
{
    public function attach(Model $model, string $relation, array $images = [], ?string $directory = null): void;
}
